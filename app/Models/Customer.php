<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    protected $table = 'customers1';


    protected $fillable=[
        'nombre',
        'email',
        'telefono',
        'num_cliente',
        'sucursal',
    ];
}
