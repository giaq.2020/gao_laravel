<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Customer;

class CreateCustomer extends Component
{
    public $nombre = '';
    public $email = '';
    public $telefono = '';
    public $num_cliente = '';
    public $sucursal = '';

    public function render()
    {
        return view('livewire.create-customer');
    }

    public function save()
    {

        $validatedData = $this->validate([
            'nombre' => 'required|string',
            'email' => 'required|email',
            'telefono' => 'required|numeric',
            'num_cliente' => 'required|numeric',
            'sucursal' => 'required|string',
        ]);

        Customer::create($validatedData);

        //dd ($this->all());
        //Customer::create($this->all());
        $this->reset();

        session()->flash('success', 'Cliente Creado');
    }
}
