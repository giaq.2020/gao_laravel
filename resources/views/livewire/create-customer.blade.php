<div>
    <div class="card offset-3 col-6">
        <div class="card-header">
          Crea Cliente
        </div>
        <div class="card-body">
            @if (session()->has('success'))
            <div class="alert alert-success">
              {{ session('success') }}
            </div>
            @endif
          
            <form wire:submit="save">
              @csrf
                <div class="mb-3">
                  <label class="form-label">Nombre</label>
                  <input wire:model="nombre" type="text" class="form-control" aria-describedby="nombreHelp">
                  @error('nombre') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="mb-3">
                    <label class="form-label">Dirección de Correo Electrónico</label>
                    <input wire:model="email" type="text" class="form-control" aria-describedby="emailHelp">
                    @error('email') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="mb-3">
                  <label class="form-label">Teléfono</label>
                  <input wire:model="telefono" type="text" class="form-control" >
                  @error('telefono') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="mb-3">
                <label class="form-label">Número de Cliente</label>
                  <input wire:model="num_cliente" type="text" class="form-control" aria-describedby="num_clienteHelp">
                  @error('num_cliente') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="mb-3">
                <label class="form-label">Sucursal</label>
                  <input wire:model="sucursal" type="text" class="form-control" aria-describedby="sucursalHelp">
                  @error('sucursal') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <button type="submit" class="btn btn-primary">Crear</button>
                <a href="/customers/" class="btn btn-primary">Regresar</a>
              </form>
              

        </div>
      </div>
</div>
