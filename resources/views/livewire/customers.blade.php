<div>
    <form>
        <button wire:navigate href="/customers/create" type="submit" class="btn btn-success btn-sm">Crear Cliente</button>
    </form>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Correo Electrónico</th>
                <th scope="col">Teléfono</th>
                <th scope="col">Número de Cliente</th>
                <th scope="col">Sucursal</th>
                <th scope="col">Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($customers as $customer)
                <tr>
                    <th scope="row">{{$customer->id}}</th>
                    <td>{{$customer->nombre}}</td>
                    <td>{{$customer->email}}</td>
                    <td>{{$customer->telefono}}</td>
                    <td>{{$customer->num_cliente}}</td>
                    <td>{{$customer->sucursal}}</td>
                    <td>
                        <button wire:navigate href="/customers/{{$customer->id}}" class="btn btn-primary btn-sm">Ver</button>
                        <button class="btn btn-secondary btn-sm">Editar</button>
                        <button wire:click="delete({{$customer->id}})" class="btn btn-danger btn-sm">Borrar</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
