<div>
    <div class="card">
        <h5 class="card-header">Ver Cliente</h5>
        <div class="card-body">
          <p class="card-text">Nombre: {{ $customer->nombre }}</p>
          <p class="card-text">Correo electrónico: {{ $customer->email }}</p>
          <p class="card-text">Teléfono: {{ $customer->telefono }}</p>
          <p class="card-text">Número de cliente: {{ $customer->num_cliente }}</p>
          <p class="card-text">Sucursal: {{ $customer->sucursal }}</p>
          <a href="/customers/" class="btn btn-primary">Regresar</a>
        </div>
      </div>
</div>
